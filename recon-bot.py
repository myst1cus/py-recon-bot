# bot.py
import os

import discord
import requests
import string
import re
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')
REGISTRATION_KEYWORDS = ["regis"]

GET_USER_ENDPOINT = "http://127.0.0.1:5000/user/{0}"


if not TOKEN:
    TOKEN = os.environ.get('DISCORD_TOKEN')

if not TOKEN:
    TOKEN = 'NzU4ODA2Nzk5NzU0MTk5MTAx.X20T5w.LQYg92EoMg26CnYUqaz_qfB989E'

client = discord.Client()

welcome_message = """Welcome {0}.

Please respond to me with the e-mail address that you'd like to register with.

Once I confirm your registration and your e-mail adress, I will grant you access to the other channels."""

@client.event
async def on_ready():
    print(f'{client.user} has connected to Discord!')
    for guild in client.guilds:
        for channel in guild.channels:
            print (f"I am connected to the {channel} channel in the {guild} guild.")

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    print("Found message in following message channel : {0}".format(message.channel))

    # here is where we'd want the logic to summon the bot to DM a user.
    # this may be a reaction in a main channel, or a channel that has instructions on what to expect.
    if 'Direct Message' not in str(message.channel) and 'register' in message.channel.name:
        for keyword in REGISTRATION_KEYWORDS:
            if keyword in message.content:
                await message.author.send(welcome_message.format(message.author.name))
                return

    if 'Direct Message' in str(message.channel):
        print("Following user ID messaged me : {0}".format(message.author.id))
        # if we are assuming the only way that a person can/should DM this bot, it's about registration, we'd want to check status right away
        # TODO: use discord person-name to hit service and pull registration status.
        # then we'll check if the message contains an e-mail address and handle that
        match_email = re.search(r'[\w\.-]+@[\w\.-]+', message.content)
        match_conf_code = re.search(r"\D(\d{8})\D", " " + message.content + " ")
        if match_email and match_email.group(0) is not None:
            await message.channel.send("Oh, cool, I see your e-mail address is {0}".format(match_email.group(0)))
            await message.channel.send("Thanks for that information.")
            response = requests.get(GET_USER_ENDPOINT.format(message.author.id))
            print("got the following json response from the server : " + str(response.json()))
            if response.status_code != 200:
                await message.channel.send("It seems I am having trouble pulling that information.")
                return
            else:
                if (response.json()["result"] is None or len(response.json()["result"]) == 0):
                    # add user
                    print ("Matching email found here is " + str(match_email.group(0)))
                    request_data = str(match_email.group(0))
                    response = requests.put(GET_USER_ENDPOINT.format(message.author.id), data=request_data)
                    print(str(response.content))
                    if response.status_code != 201:
                        await message.channel.send("It seems I can't register you at the moment. Please try again later.")
                        return
                    else:
                        # we've created a database entry and sent them a confirmation e-mail
                        await message.channel.send("""You've been successfully registered! Please check your e-mail for a confirmation code. When it arrives (it can take up to 15 minutes), past the 8-digit code here and I'll complete your registration.""")
                        return
                        
                else:
                    # this user already exists. prompt for update?
                    await message.channel.send("You are already registered with a different e-mail address. Did you want to update? Thumnbs up if yes.")
                    return
            print ("Processing registration request for {0}, using e-mail address {1}".format(message.author, match_email.group(0)))
        elif match_conf_code and match_conf_code.group(0) is not None:
            await message.channel.send("I see you are attempting to confirm with the code : " + match_conf_code.group(0))
            await message.channel.send("Please wait one moment while I verify that.")
            formatted_conf_code = str(match_conf_code.group(0)).strip()
            print ("Matching conf code found here is " + formatted_conf_code)
            response = requests.post(GET_USER_ENDPOINT.format(message.author.id)+"/confirm/"+formatted_conf_code)
            if response.status_code != 200:
                await message.channel.send("That doesn't seem to be correct. Care to try again?")
            else:
                await message.channel.send("Your e-mail has been confirmed! Head on back to the server, and I'll open the gates!")
                # TODO: give more access to server here
    return
    
@client.event
async def on_member_join(member):
    await member.send(welcome_message.format(member.name))
    print("Sent registration message to : " + member.name)


client.run(TOKEN)

